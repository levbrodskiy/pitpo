package ru.blm.shape;

import org.junit.Test;

import static org.junit.Assert.*;

public class QuadrangleTest {

    @Test
    public void isSquare() {
        Quadrangle square = new Quadrangle(new Point2D(0,3),
                new Point2D(3,3), new Point2D(3, 0), new Point2D(0,0));
        Quadrangle square2 = new Quadrangle(new Point2D(1,4),
                new Point2D(5,4), new Point2D(5, 1), new Point2D(1,1));
        assertTrue(square.isSquare());
        //assertTrue(square1.isSquare());
        assertFalse(square2.isSquare());
    }

    @Test
    public void isRectangle() {
        Quadrangle rectangle = new Quadrangle(new Point2D(0,3),
                new Point2D(3,3), new Point2D(3, 0), new Point2D(0,0));
        assertTrue(rectangle.isRectangle());
    }

    @Test
    public void isRhombus() {
        Quadrangle rhombus = new Quadrangle(new Point2D(0,3),
                new Point2D(3,3), new Point2D(3, 0), new Point2D(0,0));
        assertTrue(rhombus.isRhombus());
    }

    @Test
    public void isParallelogram() {
        Quadrangle parallelogram = new Quadrangle(new Point2D(0,3),
                new Point2D(3,3), new Point2D(3, 0), new Point2D(0,0));
        assertTrue(parallelogram.isParallelogram());
    }

    @Test
    public void isIsoscelesTrapezoid() {
        Quadrangle isoscelesTrapezoid = new Quadrangle(new Point2D(0,3),
                new Point2D(3,3), new Point2D(3, 0), new Point2D(0,0));
        assertTrue(isoscelesTrapezoid.isIsoscelesTrapezoid());
    }

    @Test
    public void isRectangularTrapezoid() {
        Quadrangle rectangularTrapezoid = new Quadrangle(new Point2D(0,3),
                new Point2D(3,3), new Point2D(3, 0), new Point2D(0,0));
        assertFalse(rectangularTrapezoid.isRectangularTrapezoid());
    }

    @Test
    public void isSimpleTrapezoid() {
        Quadrangle isoscelesTrapezoid = new Quadrangle(new Point2D(0,3),
                new Point2D(3,3), new Point2D(3, 0), new Point2D(0,0));
        assertFalse(isoscelesTrapezoid.isSimpleTrapezoid());
    }

    @Test
    public void isSimpleQuadrangle() {
        Quadrangle isSimpleQuadrangle = new Quadrangle(new Point2D(0,0),
                new Point2D(3,0), new Point2D(0, 0), new Point2D(0,0));
        assertFalse(isSimpleQuadrangle.isSimpleQuadrangle());
    }
}