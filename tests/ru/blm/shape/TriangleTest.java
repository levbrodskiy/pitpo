package ru.blm.shape;

import org.junit.Test;

import static org.junit.Assert.*;

public class TriangleTest {

    @Test
    public void isEquilateral() throws NotTriangleException {
        Triangle equilateral = new Triangle(5,5,5);
        Triangle notEquilateral = new Triangle(5,8,10);
        assertTrue(equilateral.isEquilateral());
        assertTrue(equilateral.isIsosceles());
        assertFalse(equilateral.isScalene());
        assertFalse(notEquilateral.isEquilateral());
    }

    @Test
    public void isScalene() throws NotTriangleException {
        Triangle scalene = new Triangle(10,5,8);
        Triangle notScalene = new Triangle(9,9,9);
        assertTrue(scalene.isScalene());
        assertFalse(scalene.isEquilateral());
        assertFalse(notScalene.isScalene());
    }

    @Test
    public void isIsosceles() throws NotTriangleException {
        Triangle isosceles = new Triangle(5,5,6);
        Triangle notIsosceles = new Triangle(9,8,7);
        assertTrue(isosceles.isIsosceles());
        assertFalse(notIsosceles.isIsosceles());
    }

    @Test(expected = NotTriangleException.class)
    public void isNotTriangle() throws NotTriangleException {
        Triangle notTriangle = new Triangle(7775,5,8);
    }

    @Test(expected = NotTriangleException.class)
    public void isNotTriangleNegative() throws NotTriangleException {
        Triangle notTriangle = new Triangle(-5,5,8);
    }

    @Test
    public void isScaleneAndIsosceles() throws NotTriangleException {
        Triangle scaleneAndIsosceles = new Triangle(5,5,4);
        assertTrue(scaleneAndIsosceles.isIsosceles());
        assertTrue(scaleneAndIsosceles.isScalene());
        assertFalse(scaleneAndIsosceles.isEquilateral());
    }

    @Test
    public void isRectangular() throws NotTriangleException {
        Triangle rectangular = new Triangle(3, 4, 5);
        assertTrue(rectangular.isRectangular());

        Triangle rectangular2 = new Triangle(3, 5, 4);
        assertTrue(rectangular2.isRectangular());

        Triangle rectangular3 = new Triangle(5, 3, 4);
        assertTrue(rectangular3.isRectangular());

        Triangle rectangular4 = new Triangle(7, 24, 25);
        assertTrue(rectangular4.isRectangular());

        Triangle rectangular5 = new Triangle(17, 8, 15);
        assertTrue(rectangular5.isRectangular());
    }
}