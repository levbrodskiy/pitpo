package ru.blm.equations;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

public class QuadraticEquationTest {

    @Test
    public void getSolutionWithPositiveDiscriminant() {
        try {
            QuadraticEquation quadraticEquation = new QuadraticEquation(1,3,2);
            double[] actual = {-1,-2};
            assertArrayEquals(quadraticEquation.getSolution(),actual,0);
        } catch (NoSolutionException e) {
            Assert.fail();
        }
    }

    @Test
    public void getSolutionWithZeroDiscriminant() {
        try {
            QuadraticEquation quadraticEquation = new QuadraticEquation(4,4,1);
            double[] actual = {-0.5};
            assertArrayEquals(quadraticEquation.getSolution(),actual,0);
        } catch (Exception e) {
            Assert.fail();
        }
    }

    @Test
    public void getSolutionWithNegativeDiscriminant(){
        try {
            QuadraticEquation quadraticEquation = new QuadraticEquation(400,4,1);
            Assert.fail();
        } catch (Exception e) {

        }
    }
}