import ru.blm.equations.NoSolutionException;
import ru.blm.equations.QuadraticEquation;

import java.util.InputMismatchException;
import java.util.Scanner;

public class QuadrarMain {
    public static void main(String[] args) {
        try (Scanner sc = new Scanner(System.in)){
            QuadraticEquation quadraticEquation =
                    new QuadraticEquation(sc.nextDouble(),sc.nextDouble(),sc.nextDouble());
            for (double d : quadraticEquation.getSolution()) {
                System.out.println(d);
            }
        } catch (NoSolutionException e) {
            System.out.println(e.getMessage());
        }catch (InputMismatchException e){
            System.out.println("Invalid data!");
        }
    }
}
