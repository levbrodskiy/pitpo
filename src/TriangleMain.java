import ru.blm.shape.NotTriangleException;
import ru.blm.shape.Triangle;

import java.util.InputMismatchException;
import java.util.Scanner;

public class TriangleMain {
    public static void main(String[] args) {
        try (Scanner sc = new Scanner(System.in)){
            Triangle triangle =
                    new Triangle(sc.nextInt(),sc.nextInt(),sc.nextInt());
            System.out.println("Triangle is equilateral - " + triangle.isEquilateral());
            System.out.println("Triangle is isosceles - " + triangle.isIsosceles());
            System.out.println("Triangle is scalene - " + triangle.isScalene());
            System.out.println("Triangle is rectangular - " + triangle.isRectangular());
        } catch (NotTriangleException e) {
            System.out.println(e.getMessage());
        }catch (InputMismatchException e){
            System.out.println("Invalid data!");
        }
    }
}
