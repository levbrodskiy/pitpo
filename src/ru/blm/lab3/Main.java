package ru.blm.lab3;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{
        Parent root = FXMLLoader.load(getClass().getResource("/ru/blm/lab3/views/view.fxml"));
        primaryStage.setTitle("Lab 3");
        primaryStage.setScene(new Scene(root, 560, 420));
        primaryStage.show();
    }


    public static void main(String[] args) {
        launch(args);
    }
}
