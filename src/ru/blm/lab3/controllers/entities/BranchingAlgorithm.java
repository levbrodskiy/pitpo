package ru.blm.lab3.controllers.entities;

import static java.lang.Math.*;

public class BranchingAlgorithm {
    private double x;
    private double y;
    private double functionX;

    public BranchingAlgorithm(double x, double y, Function function) {
        this.x = x;
        this.y = y;

        switch (function){
            case SIN:
                functionX = getSinFunction();
                break;

            case SQR:
                functionX = getSqrFunction();
                break;

            case EXP:
                functionX = getExpFunction();
                break;
        }
    }

    public double getResult(){
        double result = 0D;

        if (x / y > 0){
            result = log(y + 2) + functionX;
        }else if (x / y < 0){
            result = log(abs(y)) - tan(functionX);
        }else {
            result = functionX * pow(y, 3);
        }

        return result;
    }

    private double getSqrFunction(){
        return sqrt(x);
    }

    private double getSinFunction(){
        return sin(x);
    }

    private double getExpFunction(){
        return exp(x);
    }
}
