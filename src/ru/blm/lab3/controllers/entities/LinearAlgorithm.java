package ru.blm.lab3.controllers.entities;

public class LinearAlgorithm {
    private double x;
    private double y;
    private double z;

    public LinearAlgorithm(double x, double y, double z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public double getResult(){
        double firstPart = 2 * Math.cos(x - Math.PI/6);
        double secondPart = 0.5 + Math.sin(y);
        double thirdPart = 1 + (z * z) / (3 - (z * z) / 5);
        double result = (firstPart / secondPart) * thirdPart;
        return result;
    }
}
