package ru.blm.lab3.controllers.entities;

public enum Function {
    SIN, SQR, EXP;
}
