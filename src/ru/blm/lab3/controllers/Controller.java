package ru.blm.lab3.controllers;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import ru.blm.lab3.controllers.entities.BranchingAlgorithm;
import ru.blm.lab3.controllers.entities.Function;
import ru.blm.lab3.controllers.entities.LinearAlgorithm;
import ru.blm.lab3.popupwindows.ErrorWindow;

public class Controller {

    @FXML
    private TextField linearXTextField;

    @FXML
    private TextField linearYTextField;

    @FXML
    private TextField linearZTextField;

    @FXML
    private TextField branchedXTextField;

    @FXML
    private TextField branchedYTextField;

    @FXML
    private Label linearResultLabel;

    @FXML
    private Label branchedResultLabel;

    @FXML
    private RadioButton sinRB;

    @FXML
    private RadioButton expRB;

    @FXML
    private RadioButton sqrRB;

    @FXML
    void branchedCalculate(ActionEvent event) {
        double x;
        double y;
        Function function = null;
        try {
            x = Double.valueOf(branchedXTextField.getText());
            y = Double.valueOf(branchedYTextField.getText());
            if (expRB.isSelected()){
                function = Function.EXP;
            }
            if (sinRB.isSelected()){
                function = Function.SIN;
            }
            if (sqrRB.isSelected()){
                function = Function.SQR;
            }
            BranchingAlgorithm algorithm = new BranchingAlgorithm(x, y, function);
            Double result = algorithm.getResult();
            branchedResultLabel.setText(result.toString());
        }catch (Exception e){
            ErrorWindow.openWindow("Неверные данные");
            return;
        }
    }

    @FXML
    void linearCalculate(ActionEvent event) {
        double x;
        double y;
        double z;
        try {
            x = Double.valueOf(linearXTextField.getText());
            y = Double.valueOf(linearYTextField.getText());
            z = Double.valueOf(linearZTextField.getText());
            LinearAlgorithm algorithm = new LinearAlgorithm(x, y, z);
            Double result = algorithm.getResult();
            linearResultLabel.setText(result.toString());
        }catch (Exception e){
            ErrorWindow.openWindow("Неверные данные");
            return;
        }

    }

}