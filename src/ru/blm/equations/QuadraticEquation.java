package ru.blm.equations;

public class QuadraticEquation {
    private double a;
    private double b;
    private double c;
    private double D;

    public QuadraticEquation(double a, double b, double c) throws NoSolutionException {
        this.a = a;
        this.b = b;
        this.c = c;
        D = getDiscriminant();
        if (D < 0){
            throw new NoSolutionException("Equation don't have a solution!");
        }
    }

    public double[] getSolution() {
        double[] solutions;

        if (D == 0.0D){
            solutions = new double[1];
            solutions[0] = -b / (2 * a);
        }else {
            solutions = new double[2];
            solutions[0] = (-b + Math.sqrt(D)) / 2 * a;
            solutions[1] = (-b - Math.sqrt(D)) / 2 * a;
        }
        return solutions;
    }
    private double getDiscriminant(){
        return  (b * b) - (4 * a * c);
    }
}
