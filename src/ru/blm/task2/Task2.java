package ru.blm.task2;

import ru.blm.shape.Line;

import java.util.List;

public class Task2 {
    List<Line> lines;

    public Task2(List<Line> lines) {
        this.lines = lines;
    }

    public double getShadowSize(){
        for (Line line:lines) {
            for (Line l:lines) {
                if(line.containsX(l)){
                    lines.remove(l);
                }
                if (line.isCrossingX(l)){
                    line = line.getFullLineX(l);
                }
            }
        }

        double size = 0;
        for (Line sizeLine:lines) {
            double x1 = sizeLine.getPoint1().getX();
            double x2 = sizeLine.getPoint2().getX();
            if (x1 < x2){
                size += x2-x1;
            }else {
                size += x1-x2;
            }
        }
        return size;
    }


}
