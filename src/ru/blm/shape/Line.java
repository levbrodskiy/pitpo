package ru.blm.shape;

import java.util.Arrays;

public class Line {
    private Point2D point1;
    private Point2D point2;

    public Line(Point2D point1, Point2D point2) {
        this.point1 = point1;
        this.point2 = point2;
    }

    public boolean containsX(Line line){
        if(this.point1.getX() < line.point1.getX() && line.point1.getX()  < this.point2.getX() &&
            this.point1.getX() < line.point2.getX() && line.point2.getX()  < this.point2.getX()){
            return true;
        }

        if(this.point1.getX() > line.point1.getX() && line.point1.getX()  > this.point2.getX() &&
                this.point1.getX() > line.point2.getX() && line.point2.getX()  > this.point2.getX()){
            return true;
        }
            return false;
    }

    public boolean isCrossingX(Line line){
        if(this.point1.getX() < line.point1.getX() && line.point1.getX()  < this.point2.getX() ||
                this.point1.getX() < line.point2.getX() && line.point2.getX()  < this.point2.getX()){
            return true;
        }

        if(this.point1.getX() > line.point1.getX() && line.point1.getX()  > this.point2.getX() ||
                this.point1.getX() > line.point2.getX() && line.point2.getX()  > this.point2.getX()){
            return true;
        }
        return false;
    }
    public Line getFullLineX(Line line){
        double[] coordinates = {line.getPoint1().getX(), line.getPoint2().getX(), this.point1.getX(), this.point2.getX()};
        Arrays.sort(coordinates);
        return new Line(new Point2D(coordinates[0], 0), new Point2D(coordinates[3], 0));
    }
    public double getLineLength(){
        return Math.sqrt(Math.pow(point2.getX() - point1.getX(), 2) + Math.pow(point2.getY() - point1.getY(), 2));
    }

    public Point2D getPoint1() {
        return point1;
    }

    public void setPoint1(Point2D point1) {
        this.point1 = point1;
    }

    public Point2D getPoint2() {
        return point2;
    }

    public void setPoint2(Point2D point2) {
        this.point2 = point2;
    }
}
