package ru.blm.shape;

public class Triangle {

    private int a;
    private int b;
    private int c;

    public Triangle(int a, int b, int c) throws NotTriangleException {
        this.a = a;
        this.b = b;
        this.c = c;
        if (!isTriangle()){
            throw new NotTriangleException("It is not a triangle!");
        }
    }

    public boolean isEquilateral(){//равносторонний
        return a == b && a == c;
    }

    public boolean isScalene(){//неравносторонний
        return !isEquilateral();
    }

    public boolean isIsosceles(){//равнобндренный
        if (a == b || a == c || b == c){
            return true;
        }
        return false;
    }

    public boolean isRectangular(){
        return ( (a * a + c * c) == b * b) || ( (b * b + c * c) == a * a) || ( (a * a + b * b) == c * c);
    }

    private boolean isTriangle(){
        if (a <= 0 || b <= 0 || c <= 0){
            return false;
        }
        if (!(a + b > c && a + c > b && b + c > a)){
            return false;
        }
        return true;
    }
}
