package ru.blm.shape;

public class NotTriangleException extends Exception {
    public NotTriangleException(String message){
        super(message);
    }
    public NotTriangleException(){

    }
}
