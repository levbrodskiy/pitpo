package ru.blm.shape;

import static ru.blm.shape.Vector.getAngleBetweenVectors;

public class Quadrangle {
    private Point2D point1;
    private Point2D point2;
    private Point2D point3;
    private Point2D point4;
    private double a;
    private double b;
    private double c;
    private double d;
    private double angleAB;
    private double angleBC;
    private double angleCD;
    private double angleDA;

    public Quadrangle(Point2D point1, Point2D point2, Point2D point3, Point2D point4) {
        this.point1 = point1;
        this.point2 = point2;
        this.point3 = point3;
        this.point4 = point4;

         a = getLineLength(point1, point2);
         b = getLineLength(point2, point3);
         c = getLineLength(point3, point4);
         d = getLineLength(point4, point1);

         angleAB = getAngleBetweenVectors(new Vector(point2, point1), new Vector(point2, point3));
         angleBC = getAngleBetweenVectors(new Vector(point3, point2), new Vector(point3, point4));
         angleCD = getAngleBetweenVectors(new Vector(point4, point3), new Vector(point4, point1));
         angleDA = getAngleBetweenVectors(new Vector(point1, point2), new Vector(point1, point4));
    }

    public boolean isSquare(){
        System.out.println(a + " " + b + " " + c + " " + d);
        System.out.println(Math.abs(angleAB)+"/"+Math.abs(angleBC)+"/"+Math.abs(angleCD)+"/"+Math.abs(angleDA));
        return a == b && b == c && c == d && //сравнение сторон
                Math.abs(angleAB) == 0 && Math.abs(angleBC) == 0 && Math.abs(angleCD) == 0 && Math.abs(angleDA) == 0;//сравнение углов
    }

    public boolean isRectangle(){
        return a  == c && b == d && angleAB == angleBC && angleCD == angleDA ;
    }

    public boolean isRhombus(){
        return a == b && b == c && c == d && angleAB == angleCD && angleBC == angleDA;
    }

    public boolean isParallelogram(){
        return a == c && b == d && angleAB == angleCD && angleBC == angleDA;
    }

    public boolean isIsoscelesTrapezoid(){
        if (a == c && angleAB == angleBC && angleCD == angleDA){
            return true;
        }

        if (b == d && angleAB == angleDA && angleCD == angleBC){
            return true;
        }
        return false;
    }

    public boolean isRectangularTrapezoid(){
        if (angleAB == 90 && angleDA == 90 && angleBC != 90 && angleBC + angleCD == 180){
            return true;
        }

        if (angleBC == 90 && angleCD == 90 && angleAB != 90 && angleAB + angleDA == 180){
            return true;
        }

        if (angleDA == 90 && angleCD == 90 && angleBC != 90 && angleAB + angleBC == 180){
            return true;
        }

        if (angleBC == 90 && angleAB == 90 && angleCD != 90 && angleDA + angleCD == 180){
            return true;
        }

        return false;
    }

    public boolean isSimpleTrapezoid(){
        if (angleAB == angleBC || angleBC == angleCD || angleCD == angleDA){
            return false;
        }

        if ( (angleAB + angleDA == 180 && angleBC + angleCD == 180) ||
                (angleAB + angleBC == 180 && angleDA + angleCD == 180) ){
            return true;
        }
        return false;
    }

    public boolean isSimpleQuadrangle(){
        return angleAB != angleBC && angleBC != angleCD && angleCD != angleDA && angleAB + angleBC + angleCD + angleDA == 360;
    }

    private double getLineLength(Point2D point1, Point2D point2){
        return Math.sqrt(Math.pow(point2.getX() - point1.getX(), 2) + Math.pow(point2.getY() - point1.getY(), 2));
    }

}
