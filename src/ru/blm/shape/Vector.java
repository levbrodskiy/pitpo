package ru.blm.shape;

public class Vector {
    private double x;
    private double y;

    public Vector(double x, double y){
        this.x = x;
        this.y = y;
    }

    public Vector(Point2D point1, Point2D point2){
        this.x = point2.getX() - point1.getX();
        this.y = point2.getY() - point1.getY();
    }

    public static double getAngleBetweenVectors(Vector v1, Vector v2){
        double cos = ( v1.getX() * v2.getX() + v1.getY() * v2.getY() ) /
                ( Math.sqrt(v1.getX() * v1.getX() + v1.getY() * v1.getY()) * Math.sqrt(v2.getX() * v2.getX() + v2.getY() * v2.getY()) );
        return cos;
    }

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }
}
